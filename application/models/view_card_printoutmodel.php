<?php

class view_card_printoutmodel extends CI_Model {

    var $id = "";
    var $fullname_th = "";
    var $birthday = "";
    var $identification = "";
    var $blood = "";
    var $born = "";
    var $fullname_en = "";
    var $positype = "";
    var $posi = "";
    var $address = "";
    var $card_create = "";
    var $card_exp = "";
    var $birthday_th = "";
    var $card_create_th = "";
    var $card_exp_th = "";

    function get_entry() {
        $result = $this->db->get('view_card_printout');
        return $result->result();
    }

    function insert_entry() {
        $this->fullname_th = $_POST['fullname_th'];
        $this->birthday = $_POST['birthday'];
        $this->identification = $_POST['identification'];
        $this->blood = $_POST['blood'];
        $this->born = $_POST['born'];
        $this->fullname_en = $_POST['fullname_en'];
        $this->positype = $_POST['positype'];
        $this->posi = $_POST['posi'];
        $this->address = $_POST['address'];
        $this->card_create = $_POST['card_create'];
        $this->card_exp = $_POST['card_exp'];
        $this->birthday_th = $_POST['birthday_th'];
        $this->card_create_th = $_POST['card_create_th'];
        $this->card_exp_th = $_POST['card_exp_th'];
        $this->db->insert('view_card_printout', $this);
    }

    function update_entry($id) {
        $this->fullname_th = $_POST['fullname_th'];
        $this->birthday = $_POST['birthday'];
        $this->identification = $_POST['identification'];
        $this->blood = $_POST['blood'];
        $this->born = $_POST['born'];
        $this->fullname_en = $_POST['fullname_en'];
        $this->positype = $_POST['positype'];
        $this->posi = $_POST['posi'];
        $this->address = $_POST['address'];
        $this->card_create = $_POST['card_create'];
        $this->card_exp = $_POST['card_exp'];
        $this->birthday_th = $_POST['birthday_th'];
        $this->card_create_th = $_POST['card_create_th'];
        $this->card_exp_th = $_POST['card_exp_th'];
        $this->db->update('view_card_printout', $this, array('id' => $id));
    }

}
