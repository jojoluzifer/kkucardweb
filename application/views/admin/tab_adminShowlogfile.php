<div class="tab-pane fade" id="print">
    <form id="tab2" class="user-layout">
        <table class="table table-bordered" >
            <tr >
                <td>
                    <i class="icon-print">
                    </i><?php echo anchor('admin/showdata', ' พิมพ์รายงานการแก้ไขข้อมูลของบุคลาการ') ?>
                </td>
            </tr>
        </table>

        <table class="table table-bordered">

            <tr>
                <th>ชื่อ-สกุล</th>
                <th>เลขบัตรประชาชน </th>
                <th>ประเภท</th>
                <th>หมวดงาน</th>
                <th>สายงาน</th>
                <th>ตำแหน่งงาน</th>
                <th>ปรับแต่ง</th>
            </tr>

            <?php
            foreach ($results_c as $data) {
                ?>
                <tr>
                    <td><?php echo $data->title ?>&nbsp;<?php echo $data->firstname ?>&nbsp;<?php echo $data->lastname ?></td>
                    <td><?php echo $data->identification ?></td>
                    <td><?php echo $data->positype ?></td>
                    <td><?php echo $data->block ?></td>
                    <td><?php echo $data->workline ?></td>
                    <td><?php echo $data->posi ?></td>
                    <td><a href="<?php echo base_url() ?>admin/confirm_card/<?php echo $data->id_card ?>">Confirm ข้อมูล</a></td>
                </tr>


                <?php
            }
            ?>

        </table> 
        <p style="margin-bottom: 15px;"><?php echo $links_c; ?></p>
        <i class="icon-print"></i>
        <?php echo anchor('admin/showdata', ' พิมพ์รายงานการแก้ไขข้อมูลของบุคลาการ') ?>

    </form>
</div>