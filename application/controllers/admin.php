<?php

class Admin extends CI_Controller {

    public function index($error = false) {

        $id = $this->session->userdata('admin_id');
        if ($id)
            redirect("admin/adminProfile", "refresh");

        $data['page_title'] = '.:: เข้าสู่ระบบ KKU Administor ::.';
        $data['nav_title'] = ' KKU CARD : ADMINISTRATOR';

        if ($error)
            $data['error'] = "***** ชื่อผู้ใช้งาน หรือ รหัสผ่านของคุณไม่ถูกต้อง *****";
        else
            $data['error'] = "";

        $this->load->view('template/head', $data);
        $this->load->view('template/2_navbar', $data);
        $this->load->view('admin/index');
        $this->load->view('template/footer');
    }

    public function adminProfile() {
        $id = $this->session->userdata('admin_id');
        if (!$id)
            redirect("", "refresh");

        $page_data['page_title'] = '.:: การจัดการข้อมูลของผู้ใช้งาน ::.';
        $data['nav_title'] = ' KKU CARD : ADMINISTRATOR';
        $data['admin_id'] = $id;
        $this->load->model('user_adminmodel', 'model_user_admin');
        $data['user_admin'] = $this->model_user_admin->get_one_entry($id);

        $this->load->model('logfilemodel');


// ###############        data for logfile  ############### 

        $config = array();
        $config["base_url"] = base_url() . "admin/adminProfile";
        $config["total_rows"] = $this->logfilemodel->record_data();
        $config["per_page"] = 15;
        $config["uri_segment"] = 3;

        $title = isset($_POST['title']) ? $_POST['title'] : "";
        $iden = isset($_POST['identification']) ? $_POST['identification'] : "";
        $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : "";
        $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : "";
        $time = isset($_POST['time']) ? $_POST['time'] : "";

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->logfilemodel->fetch_logfile($config["per_page"], $page, $title, $iden, $firstname, $lastname, $time);

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        // ###############     end of  data for logfile  ############### 


        $this->load->view('template/head', $page_data);
        $this->load->view('template/tab-admin');
        $this->load->view('template/2_navbar', $data);
        //  $this->load->view('admin/adminProfile', $data);
        $this->parser->parse('admin/adminProfile', $data);
    }

    function admin_show_cardid() {
        $id = $this->session->userdata('admin_id');

        if (!$id)
            redirect("", "refresh");

        $page_data['page_title'] = '.:: การจัดการข้อมูลบัตรใน Card_id ::.';
        $data['nav_title'] = ' KKU CARD : ADMINISTRATOR';
        $data['admin_id'] = $id;
        $this->load->model('user_adminmodel', 'model_user_admin');
        $data['user_admin'] = $this->model_user_admin->get_one_entry($id);


        $this->load->model('card_id_systemmodel');

        // ###############        data for card_id_systemmodel       ############### 

        $config_c = array();
        $config_c["base_url"] = base_url() . "admin/admin_show_cardid";
        $config_c["total_rows"] = $this->card_id_systemmodel->record_data();
        $config_c["per_page"] = 15;
        $config_c["uri_segment"] = 3;

        $title = isset($_POST['title']) ? $_POST['title'] : "";
        $iden = isset($_POST['identification']) ? $_POST['identification'] : "";
        $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : "";
        $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : "";

        $page2 = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results_c"] = $this->card_id_systemmodel->fetch_cardid($config_c ["per_page"], $page2, $title, $iden, $firstname, $lastname);

        $this->pagination->initialize($config_c);

        $data["links_c"] = $this->pagination->create_links();

//  ###############     end of  data for card_id_systemmodel     ############### 



        $this->load->view('template/head', $page_data);
        $this->load->view('template/2_navbar', $data);
        $this->load->view('template/tab-admin');
        //  $this->load->view('admin/adminProfile', $data);
        $this->parser->parse('admin/admin_card_id', $data);
    }

    function admin_show_bug() {

        $id = $this->session->userdata('admin_id');

        if (!$id)
            redirect("", "refresh");


        $page_data['page_title'] = '.:: การจัดการข้อมูลข้อผิดพลาด ::.';
        $data['nav_title'] = ' KKU CARD : ADMINISTRATOR';
        $data['admin_id'] = $id;
        $this->load->model('user_adminmodel', 'model_user_admin');
        $data['user_admin'] = $this->model_user_admin->get_one_entry($id);

        $this->load->model('bug_model');


        // ###############        data for bug_idmodel       ############### 

        $config_b = array();
        $config_b["base_url"] = base_url() . "admin/admin_show_bug";
        $config_b["total_rows"] = $this->bug_model->record_data();
        $config_b["per_page"] = 15;
        $config_b["uri_segment"] = 3;

        $title = isset($_POST['title']) ? $_POST['title'] : "";
        $iden = isset($_POST['identification']) ? $_POST['identification'] : "";
        $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : "";
        $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : "";
        $time = isset($_POST['time']) ? $_POST['time'] : "";

        $page3 = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results_b"] = $this->bug_model->fetch_bug($config_b ["per_page"], $page3, $title, $iden, $firstname, $lastname, $time);

        $this->pagination->initialize($config_b);

        $data["links_b"] = $this->pagination->create_links();

//  ###############     end of  data for bug_idmodel     ############### 


        $this->load->view('template/head', $page_data);
        $this->load->view('template/2_navbar', $data);
        $this->load->view('template/tab-admin');
        //  $this->load->view('admin/adminProfile', $data);
        $this->parser->parse('admin/admin_bug', $data);
    }

    public function login() {
        $data['page_title'] = '.:: KKU Administor ::.';

        $this->load->model('user_adminmodel', 'model');
        $data['user_admin'] = $this->model->login();
        $this->session->set_userdata('admin_id', $data['user_admin'][0]->id);

        redirect("admin/adminProfile", "refresh");
    }

    function logout() {
        $this->session->unset_userdata('admin_id');

        redirect('/admin', 'refresh');
    }

    public function showdata() {
        $this->load->model('card_id_systemmodel', 'model_showdata');
        $data['card_id_showdata'] = $this->model_showdata->get_entry();
        $data['page_title'] = '.:: พิมพ์รายงานการเข้าใช้ของบุคลาการ ::.';

        $this->load->view('template/head', $data);
        $this->parser->parse('admin/adminShowdata', $data);
    }

    public function showlog() {
        $this->load->model('logfilemodel', 'model_showlog');
        $data['card_id_showlog'] = $this->model_showlog->get_entry_where();
        $data['page_title'] = '.:: พิมพ์รายงานการแก้ไขข้อมูลของบุคลาการ ::.';

        $this->load->view('template/head', $data);
        $this->parser->parse('admin/adminShowlogfile', $data);
    }

    public function confirm_card($id) {

        $data['page_title'] = '.:: Update รายงานการแก้ไขข้อมูลของบุคลาการ ::.';
        // ******************************************* 18-01-2556 ********************************************************************** จากเดิม
        // $data['admin_id'] = $id;
        $data['id'] = $id;


        $this->load->model('card_id_systemmodel', 'model');
        $data['user_data'] = $this->model->get_one_entry($id);

        $this->load->view('template/head', $data);
        $this->load->view('admin/admin_confirm_card', $data);
    }

    public function show_logfile($id) {

        $data['page_title'] = '.::  แสดงรายงานการเข้าใช้ของบุคลาการ ::.';
        $data['admin_id'] = $id;

        $this->load->model('logfilemodel', 'model_logfile');
        $data['log_id'] = $this->model_logfile->get_one_entry_where($id);
        //   print_r($data['log_id']);
        $this->load->view('template/head', $data);
        $this->parser->parse('admin/admin_show_logfile', $data);
    }

    public function admin_update_member($id) {

        $data['page_title'] = '.::  Admin อัพเดตข้อมูลผู้ใช้ ::.';
        $data['modal_text'] = 'ยืนยัน ข้อมูลบุคลากร เสร็จสิ้น...';
        $this->load->view('template/head', $data);

        $this->load->model('card_idmodel');
        $this->load->model('card_id_systemmodel');

        $this->card_idmodel->admin_update($id);
        $this->card_id_systemmodel->delete_entry($id);

        $this->load->view('admin/show_text_confirm', $data);
        //  redirect('/admin/adminProfile/', 'refresh');
    }

    public function adminShowbug($id) {

        $data['page_title'] = '.::  รายงานการแจ้งข้อผิดพลาด ::.';
        $data['admin_id'] = $id;

        $this->load->model('bug_model', 'model_bug');
        $data['bug'] = $this->model_bug->get_one_entry_where($id);
        //   print_r($data['log_id']);
        $this->load->view('template/head', $data);
        $this->parser->parse('admin/admin_show_bug', $data);
    }

    public function search_logfile() {
        
    }

    public function search_card() {
        
    }

    public function search_bug() {
        
    }

}

?>
