<!DOCTYPE html>
<html lang="th">  
    <head>
        <title>Print Out : [<?= $result[0]->id ?> - <?= $result[0]->title ?> <?= $result[0]->firstname ?> <?= $result[0]->lastname ?>] </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >

        <style type="text/css">
            div,div table,div td,div p{
                font-family: 'Michroma', arial, serif;
                font-size: 1em;
            }
        </style>

    </head>
    <body>
        <h3 align="center">แบบยืนยันข้อมูลสำหรับการออกบัตร</h3>
        <div style="border:1px solid #000; width:100%; padding: 10px;">
            <b>ข้อมูลส่วนตัว</b>
        </div>
        <div style="border:1px solid #000; border-top: none; border-bottom: none; width:100%;">
            <table width="100%;" cellspacing="0" cellpadding="8">
                <tr valign="top">
                    <td>คำนำหน้าชื่อ :</td><td><?= $result[0]->title ?></td>
                </tr>
                <tr valign="top">
                    <td>ชื่อจริง :</td><td><?= $result[0]->firstname ?></td>
                    <td>นามสกุล :</td><td><?= $result[0]->lastname ?></td>
                </tr>
                <tr valign="top">
                    <td>Title :</td><td colspan="3"><?= $result[0]->title_eng ?></td>
                </tr>
                <tr valign="top">
                    <td width="20%">First Name :</td><td width="30%"><?= $result[0]->firstname_eng ?></td>
                    <td width="20%">Last Name :</td><td width="30%"><?= $result[0]->lastname_eng ?></td>
                </tr>
                <tr valign="top">
                    <td>เพศ :</td><td><?= $result[0]->sex ?></td>
                    <td>เลขบัตรประชาชน :</td><td><?= $result[0]->identification ?></td>
                </tr>
                <tr valign="top">
                    <td>วันเกิด :</td><td><?= $result[0]->birthday ?></td>
                    <td>กรุ๊ปเลือด :</td><td><?= $result[0]->blood ?></td>
                </tr>
                <tr valign="top">
                    <td>สถานที่เกิด :</td><td><?= $result[0]->born ?></td>
                    <td>ศาสนา :</td><td><?= $result[0]->religion ?></td>
                </tr>
                <tr valign="top">
                    <td>สัญชาติ :</td><td><?= $result[0]->nation ?></td>
                    <td>สถานะ :</td><td><?= $result[0]->statuslist ?></td>
                </tr>
            </table>
        </div>
        <div style="border:1px solid #000; border-top: none; border-bottom: none; width:100%;">
            <table  width="100%" cellspacing="0" cellpadding="8" border="0">
                <tr valign="top">
                    <td width="15%">บ้านเลขที่ :</td><td width="15%"><?= $result[0]->homeadd ?></td>
                    <td width="10%">หมู่ :</td><td width="10%"><?= $result[0]->moo ?></td>
                    <td width="10%">ตรอก :</td><td width="10%"><?= $result[0]->tok ?></td>
                    <td width="10%">ซอย :</td><td width="20%"><?= $result[0]->soi ?></td>

                </tr>
            </table>
        </div>
        <div style="border:1px solid #000; border-top: none; border-bottom: none; width:100%;">
            <table  width="100%" cellspacing="0" cellpadding="8">
                <tr valign="top">
                    <td width="15%">ถนน : </td><td width="15%"><?= $result[0]->street ?></td>
                    <td width="10%">ตำบล : </td><td width="20%"><?= $result[0]->district ?></td>
                    <td width="10%">อำเภอ : </td><td width="30%"><?= $result[0]->amphur ?></td>
                </tr>
            </table>
        </div>
        <div style="border:1px solid #000; border-top: none; border-bottom: none; width:100%;">
            <table  width="100%" cellspacing="0" cellpadding="8">
                <tr valign="top">
                    <td width="15%">จังหวัด :</td><td width="15%"><?= $result[0]->province ?></td>
                    <td width="18%">รหัสไปรษณีย์ :</td><td width="52%"><?= $result[0]->zipcode ?></td>
                </tr>
            </table>
        </div>
        <div style="border:1px solid #000; border-top: none; border-bottom: none; width:100%;">
            <table  width="100%" cellspacing="0" cellpadding="8">
                <tr valign="top">
                    <td width="20%">E-Mail :</td><td width="30%"><?= $result[0]->email ?></td>
                    <td width="20%">เบอร์โทร :</td><td width="30%"><?= $result[0]->telephone ?></td>
                </tr>
            </table>
        </div>
        <div style="width:100% ; border: 1px solid #000; padding:10px;">
            <b>ข้อมูลการทำงาน</b>
        </div>
        <div style="border:1px solid #000; border-top: none;  width:100%;">
            <table width="100%" cellspacing="0" cellpadding="8">
                <tr>
                    <td width="25%">เริ่มงานวันที่ :</td><td width="25%"><?= $result[0]->putday ?></td>
                    <td width="25%">ประเภท :</td><td width="25%"><?= $result[0]->positype ?></td>
                </tr>
                <tr>
                    <td width="25%">สายงาน :</td><td width="25%"><?= $result[0]->workline ?></td>
                    <td width="25%">ตำแหน่งงาน :</td><td width="25%"><?= $result[0]->posi ?></td>
                </tr>
                <tr>
                    <td width="25%">คณะ :</td><td width="25%"><?= $result[0]->faculty ?></td>
                    <td width="25%">หน่วยงาน :</td><td width="25%"><?= $result[0]->division ?></td>
                </tr>
                <tr>
                    <td width="25%">หมวดงาน :</td><td width="25%"><?= $result[0]->block ?></td>
                    <td width="25%">ระดับ :</td><td width="25%"><?= $result[0]->level ?></td>
                </tr>
                <tr>
                    <td width="25%">JOB : </td><td width="25%"><?= $result[0]->job ?></td>
                    <td width="25%">UNIT : </td><td width="25%"><?= $result[0]->unit ?></td>
                </tr>
            </table>
        </div>

        <div style="position: fixed; bottom: 120px; right: 0; width:200px; height: 20px; text-align: center;">
            ลงชื่อ
        </div>

        <div style="position: fixed; bottom: 32px; right: -5px; width:210px; height: 70px; border: 1px solid #000;">

        </div>
        <div style="position: fixed; bottom: 0; right: 0; width:200px; height: 20px; text-align: center;">
            (<?= $result[0]->title ?> <?= $result[0]->firstname ?> <?= $result[0]->lastname ?>)
        </div>
    </body>
</html>