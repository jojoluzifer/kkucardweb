<?php

class card_idmodel extends CI_Model {

    var $id = "";
    var $title = "";
    var $firstname = "";
    var $lastname = "";
    var $sex = "";
    var $birthday = "";
    var $identification = "";
    var $blood = "";
    var $born = "";
    var $nation = "";
    var $religion = "";
    var $statuslist = "";
    var $title_eng = "";
    var $firstname_eng = "";
    var $lastname_eng = "";
    var $putday = "";
    var $positype = "";
    var $block = "";
    var $workline = "";
    var $posi = "";
    var $level = "";
    var $faculty = "";
    var $division = "";
    var $job = "";
    var $unit = "";
    var $homeadd = "";
    var $moo = "";
    var $tok = "";
    var $soi = "";
    var $street = "";
    var $district = "";
    var $amphur = "";
    var $province = "";
    var $telephone = "";
    var $zipcode = "";
    var $email = "";
    var $usr = "";
    var $time = "";
    var $card_create = "";
    var $card_exp = "";

    function get_entry() {
        $result = $this->db->get('card_id', 10);
        return $result->result();
    }

    function check_entry() {
        $this->id = $id;

        $where = array('id' => $this->id);

        $query = $this->db->get_where('card_id', $where);
        return $query->row();
    }

    function get_one_entry($id) {

        $this->id = $id;

        $where = array('id' => $this->id);

        $query = $this->db->get_where('card_id', $where);
        return $query->result();
    }

    function insert_entry() {



        $this->id = $_POST['id'];
        $this->title = $_POST['title'];
        $this->firstname = $_POST['firstname'];
        $this->lastname = $_POST['lastname'];
        $this->sex = $_POST['sex'];
//        $this->birthday = $_POST['birthday'];
//        $this->identification = $_POST['identification'];
        $this->blood = $_POST['blood'];
        $this->born = $_POST['born'];
        $this->nation = $_POST['nation'];
        $this->religion = $_POST['religion'];
        $this->statuslist = $_POST['statuslist'];
        $this->title_eng = $_POST['title_eng'];
        $this->firstname_eng = $_POST['firstname_eng'];
        $this->lastname_eng = $_POST['lastname_eng'];
//        $this->putday = $_POST['putday'];
        $this->positype = $_POST['positype'];
        $this->block = $_POST['block'];
        $this->workline = $_POST['workline'];
        $this->posi = $_POST['posi'];
        $this->level = $_POST['level'];
        $this->faculty = $_POST['faculty'];
        $this->division = $_POST['division'];
        $this->job = $_POST['job'];
        $this->unit = $_POST['unit'];
        $this->homeadd = $_POST['homeadd'];
        $this->moo = $_POST['moo'];
        $this->tok = $_POST['tok'];
        $this->soi = $_POST['soi'];
//        $this->street = $_POST['street'];
//        $this->district = $_POST['district'];
        $this->amphur = $_POST['amphur'];
        $this->province = $_POST['province'];
        $this->telephone = $_POST['telephone'];
        $this->zipcode = $_POST['zipcode'];
        $this->email = $_POST['email'];
//        $this->usr = $_POST['usr'];
//        $this->time = $_POST['time'];
//        $this->card_create = $_POST['card_create'];
//        $this->card_exp = $_POST['card_exp'];
        $this->db->insert('card_id', $this);
    }

    function update_entry($id) {
        $this->title = $_POST['title'];
        $this->firstname = $_POST['firstname'];
        $this->lastname = $_POST['lastname'];
        $this->sex = $_POST['sex'];
        $this->birthday = $_POST['birthday'];
        $this->identification = $_POST['identification'];
        $this->blood = $_POST['blood'];
        $this->born = $_POST['born'];
        $this->nation = $_POST['nation'];
        $this->religion = $_POST['religion'];
        $this->statuslist = $_POST['statuslist'];
        $this->title_eng = $_POST['title_eng'];
        $this->firstname_eng = $_POST['firstname_eng'];
        $this->lastname_eng = $_POST['lastname_eng'];
        $this->putday = $_POST['putday'];
        $this->positype = $_POST['positype'];
        $this->block = $_POST['block'];
        $this->workline = $_POST['workline'];
        $this->posi = $_POST['posi'];
        $this->level = $_POST['level'];
        $this->faculty = $_POST['faculty'];
        $this->division = $_POST['division'];
        $this->job = $_POST['job'];
        $this->unit = $_POST['unit'];
        $this->homeadd = $_POST['homeadd'];
        $this->moo = $_POST['moo'];
        $this->tok = $_POST['tok'];
        $this->soi = $_POST['soi'];
        $this->street = $_POST['street'];
        $this->district = $_POST['district'];
        $this->amphur = $_POST['amphur'];
        $this->province = $_POST['province'];
        $this->telephone = $_POST['telephone'];
        $this->zipcode = $_POST['zipcode'];
        $this->email = $_POST['email'];
        $this->usr = $_POST['usr'];
        $this->time = $_POST['time'];
        $this->card_create = $_POST['card_create'];
        $this->card_exp = $_POST['card_exp'];
        $this->db->update('card_id', $this, array('id' => $id));
    }

}