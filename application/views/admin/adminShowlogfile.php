<table class="table table-bordered">
    <tr>
        <th>ชื่อ-สกุล</th>
        <th>เลขบัตรประชาชน </th>
        <th>ประเภท</th>
        <th>หมวดงาน</th>
        <th>สายงาน</th>
        <th>ตำแหน่งงาน</th>
        <th>วัน เวลา</th>
    </tr>
    {card_id_showlog}
    <tr>
        <td>{title}&nbsp;{firstname}&nbsp;{lastname}</td>
        <td>{identification}</td>
        <td>{positype}</td>
        <td>{block}</td>
        <td>{workline}</td>
        <td>{posi}</td>
        <td>{date_time}</td>
    </tr>
{/card_id_showlog}
</table>