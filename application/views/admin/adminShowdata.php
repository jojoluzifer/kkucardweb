<table class="table table-bordered">
    <tr>
        <th>ชื่อ-สกุล</th>
        <th>เลขบัตรประชาชน </th>
        <th>ประเภท</th>
        <th>หมวดงาน</th>
        <th>สายงาน</th>
        <th>ตำแหน่งงาน</th>
    </tr>
    {card_id_showdata}
    <tr>
        <td>{title}&nbsp;{firstname}&nbsp;{lastname}</td>
        <td>{identification}</td>
        <td>{positype}</td>
        <td>{block}</td>
        <td>{workline}</td>
        <td>{posi}</td>
    </tr>
    {/card_id_showdata}
</table>