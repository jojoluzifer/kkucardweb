<?php

class card_id_systemmodel extends CI_Model {

    var $id = "";
    var $title = "";
    var $firstname = "";
    var $lastname = "";
    var $sex = "";
    var $birthday = "";
    var $identification = "";
    var $blood = "";
    var $born = "";
    var $nation = "";
    var $religion = "";
    var $statuslist = "";
    var $title_eng = "";
    var $firstname_eng = "";
    var $lastname_eng = "";
    var $putday = "";
    var $positype = "";
    var $block = "";
    var $workline = "";
    var $posi = "";
    var $level = "";
    var $faculty = "";
    var $division = "";
    var $job = "";
    var $unit = "";
    var $homeadd = "";
    var $moo = "";
    var $tok = "";
    var $soi = "";
    var $street = "";
    var $district = "";
    var $amphur = "";
    var $province = "";
    var $telephone = "";
    var $zipcode = "";
    var $email = "";
    var $usr = "";
    var $time = "";
    var $card_create = "";
    var $card_exp = "";

    function get_entry() {
        $result = $this->db->get('card_id_system', 10);
        return $result->result();
    }

    function check_entry($id) {
        $this->id = $id;

        $where = array('id' => $this->id);

        $query = $this->db->get_where('card_id_system', $where);

        return $query->num_rows();
    }

    function get_one_entry($id) {

        $this->id = $id;

        $where = array('id' => $this->id);

        $query = $this->db->get_where('card_id_system', $where);


        return $query->result();
    }

    function insert_entry() {


        $title = '';
        $firstname = '';
        $lastname = '';
        $sex = '';
        $birthday = '';
        $identification = '';
        $blood = '';
        $born = '';
        $nation = '';
        $religion = '';
        $statuslist = '';
        $title_eng = '';
        $firstname_eng = '';
        $lastname_eng = '';
        $putday = '';
        $positype = '';
        $block = '';
        $workline = '';
        $posi = '';
        $level = '';
        $faculty = '';
        $division = '';
        $unit = '';
        $homeadd = '';
        $moo = '';
        $tok = '';
        $soi = '';
        $job = '';
        $street = '';
        $district = '';
        $amphur = '';
        $province = '';
        $telephone = '';
        $zipcode = '';
        $email = '';



        $_POST['title'] == "-" ? $title = NULL : $title = $_POST['title'];
        $_POST['firstname'] == "-" ? $firstname = NULL : $firstname = $_POST['firstname'];
        $_POST['lastname'] == "-" ? $lastname = NULL : $lastname = $_POST['lastname'];
        $_POST['sex'] == "-" ? $sex = NULL : $sex = $_POST['sex'];
        $_POST['birthday'] == "-" ? $birthday = NULL : $birthday = $_POST['birthday'];
        $_POST['identification'] == "-" ? $identification = NULL : $identification = $_POST['identification'];
        $_POST['blood'] == "-" ? $blood = NULL : $blood = $_POST['blood'];
        $_POST['born'] == "-" ? $born = NULL : $born = $_POST['born'];
        $_POST['nation'] == "-" ? $nation = NULL : $nation = $_POST['nation'];
        $_POST['religion'] == "-" ? $religion = NULL : $religion = $_POST['religion'];
        $_POST['statuslist'] == "-" ? $statuslist = NULL : $statuslist = $_POST['statuslist'];
        $_POST['title_eng'] == "-" ? $title_eng = NULL : $title_eng = $_POST['title_eng'];
        $_POST['firstname_eng'] == "-" ? $firstname_eng = NULL : $firstname_eng = $_POST['firstname_eng'];
        $_POST['lastname_eng'] == "-" ? $lastname_eng = NULL : $lastname_eng = $_POST['lastname_eng'];
        $_POST['putday'] == "-" ? $putday = NULL : $putday = $_POST['putday'];
        $_POST['positype'] == "-" ? $positype = NULL : $positype = $_POST['positype'];
        $_POST['block'] == "-" ? $block = NULL : $block = $_POST['block'];
        $_POST['workline'] == "-" ? $workline = NULL : $workline = $_POST['workline'];
        $_POST['posi'] == "-" ? $posi = NULL : $posi = $_POST['posi'];
        $_POST['level'] == "-" ? $level = NULL : $level = $_POST['level'];
        $_POST['faculty'] == "-" ? $faculty = NULL : $faculty = $_POST['faculty'];
        $_POST['division'] == "-" ? $division = NULL : $division = $_POST['division'];
        $_POST['job'] == "-" ? $job = NULL : $job = $_POST['job'];
        $_POST['unit'] == "-" ? $unit = NULL : $unit = $_POST['unit'];
        $_POST['homeadd'] == "-" ? $homeadd = NULL : $homeadd = $_POST['homeadd'];
        $_POST['moo'] == "-" ? $moo = NULL : $moo = $_POST['moo'];
        $_POST['tok'] == "-" ? $tok = NULL : $tok = $_POST['tok'];
        $_POST['soi'] == "-" ? $soi = NULL : $soi = $_POST['soi'];
        $_POST['street'] == "-" ? $street = NULL : $street = $_POST['street'];
        $_POST['district'] == "-" ? $district = NULL : $district = $_POST['district'];
        $_POST['amphur'] == "-" ? $amphur = NULL : $amphur = $_POST['amphur'];
        $_POST['province'] == "-" ? $province = NULL : $province = $_POST['province'];
        $_POST['telephone'] == "-" ? $telephone = NULL : $telephone = $_POST['telephone'];
        $_POST['zipcode'] == "-" ? $zipcode = NULL : $zipcode = $_POST['zipcode'];
        $_POST['email'] == "-" ? $email = NULL : $email = $_POST['email'];



        $this->id = $_POST['id'];
        $this->title = $title;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->sex = $sex;
        $this->birthday = $birthday;
        $this->identification = $identification;
        $this->blood = $blood;
        $this->born = $born;
        $this->nation = $nation;
        $this->religion = $religion;
        $this->statuslist = $statuslist;
        $this->title_eng = $title_eng;
        $this->firstname_eng = $firstname_eng;
        $this->lastname_eng = $lastname_eng;
        $this->putday = $putday;
        $this->positype = $positype;
        $this->block = $block;
        $this->workline = $workline;
        $this->posi = $posi;
        $this->level = $level;
        $this->faculty = $faculty;
        $this->division = $division;
        $this->job = $job;
        $this->unit = $unit;
        $this->homeadd = $homeadd;
        $this->moo = $moo;
        $this->tok = $tok;
        $this->soi = $soi;
        $this->street = $street;
        $this->district = $district;
        $this->amphur = $amphur;
        $this->province = $province;
        $this->telephone = $telephone;
        $this->zipcode = $zipcode;
        $this->email = $email;
//        $this->usr = $_POST['usr'];
//        $this->time = $_POST['time'];
//        $this->card_create = $_POST['card_create'];
//        $this->card_exp = $_POST['card_exp'];
        $this->db->insert('card_id_system', $this);
    }

    function update_entry($id) {
        $title = '';
        $firstname = '';
        $lastname = '';
        $sex = '';
        $birthday = '';
        $identification = '';
        $blood = '';
        $born = '';
        $nation = '';
        $religion = '';
        $statuslist = '';
        $title_eng = '';
        $firstname_eng = '';
        $lastname_eng = '';
        $putday = '';
        $positype = '';
        $block = '';
        $workline = '';
        $posi = '';
        $level = '';
        $faculty = '';
        $division = '';
        $unit = '';
        $homeadd = '';
        $moo = '';
        $tok = '';
        $soi = '';
        $job = '';
        $street = '';
        $district = '';
        $amphur = '';
        $province = '';
        $telephone = '';
        $zipcode = '';
        $email = '';



        $_POST['title'] == "-" ? $title = NULL : $title = $_POST['title'];
        $_POST['firstname'] == "-" ? $firstname = NULL : $firstname = $_POST['firstname'];
        $_POST['lastname'] == "-" ? $lastname = NULL : $lastname = $_POST['lastname'];
        $_POST['sex'] == "-" ? $sex = NULL : $sex = $_POST['sex'];
        $_POST['birthday'] == "-" ? $birthday = NULL : $birthday = $_POST['birthday'];
        $_POST['identification'] == "-" ? $identification = NULL : $identification = $_POST['identification'];
        $_POST['blood'] == "-" ? $blood = NULL : $blood = $_POST['blood'];
        $_POST['born'] == "-" ? $born = NULL : $born = $_POST['born'];
        $_POST['nation'] == "-" ? $nation = NULL : $nation = $_POST['nation'];
        $_POST['religion'] == "-" ? $religion = NULL : $religion = $_POST['religion'];
        $_POST['statuslist'] == "-" ? $statuslist = NULL : $statuslist = $_POST['statuslist'];
        $_POST['title_eng'] == "-" ? $title_eng = NULL : $title_eng = $_POST['title_eng'];
        $_POST['firstname_eng'] == "-" ? $firstname_eng = NULL : $firstname_eng = $_POST['firstname_eng'];
        $_POST['lastname_eng'] == "-" ? $lastname_eng = NULL : $lastname_eng = $_POST['lastname_eng'];
        $_POST['putday'] == "-" ? $putday = NULL : $putday = $_POST['putday'];
        $_POST['positype'] == "-" ? $positype = NULL : $positype = $_POST['positype'];
        $_POST['block'] == "-" ? $block = NULL : $block = $_POST['block'];
        $_POST['workline'] == "-" ? $workline = NULL : $workline = $_POST['workline'];
        $_POST['posi'] == "-" ? $posi = NULL : $posi = $_POST['posi'];
        $_POST['level'] == "-" ? $level = NULL : $level = $_POST['level'];
        $_POST['faculty'] == "-" ? $faculty = NULL : $faculty = $_POST['faculty'];
        $_POST['division'] == "-" ? $division = NULL : $division = $_POST['division'];
        $_POST['job'] == "-" ? $job = NULL : $job = $_POST['job'];
        $_POST['unit'] == "-" ? $unit = NULL : $unit = $_POST['unit'];
        $_POST['homeadd'] == "-" ? $homeadd = NULL : $homeadd = $_POST['homeadd'];
        $_POST['moo'] == "-" ? $moo = NULL : $moo = $_POST['moo'];
        $_POST['tok'] == "-" ? $tok = NULL : $tok = $_POST['tok'];
        $_POST['soi'] == "-" ? $soi = NULL : $soi = $_POST['soi'];
        $_POST['street'] == "-" ? $street = NULL : $street = $_POST['street'];
        $_POST['district'] == "-" ? $district = NULL : $district = $_POST['district'];
        $_POST['amphur'] == "-" ? $amphur = NULL : $amphur = $_POST['amphur'];
        $_POST['province'] == "-" ? $province = NULL : $province = $_POST['province'];
        $_POST['telephone'] == "-" ? $telephone = NULL : $telephone = $_POST['telephone'];
        $_POST['zipcode'] == "-" ? $zipcode = NULL : $zipcode = $_POST['zipcode'];
        $_POST['email'] == "-" ? $email = NULL : $email = $_POST['email'];



        $this->id = $_POST['id'];
        $this->title = $title;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->sex = $sex;
        $this->birthday = $birthday;
        $this->identification = $identification;
        $this->blood = $blood;
        $this->born = $born;
        $this->nation = $nation;
        $this->religion = $religion;
        $this->statuslist = $statuslist;
        $this->title_eng = $title_eng;
        $this->firstname_eng = $firstname_eng;
        $this->lastname_eng = $lastname_eng;
        $this->putday = $putday;
        $this->positype = $positype;
        $this->block = $block;
        $this->workline = $workline;
        $this->posi = $posi;
        $this->level = $level;
        $this->faculty = $faculty;
        $this->division = $division;
        $this->job = $job;
        $this->unit = $unit;
        $this->homeadd = $homeadd;
        $this->moo = $moo;
        $this->tok = $tok;
        $this->soi = $soi;
        $this->street = $street;
        $this->district = $district;
        $this->amphur = $amphur;
        $this->province = $province;
        $this->telephone = $telephone;
        $this->zipcode = $zipcode;
        $this->email = $email;
//        $this->usr = $_POST['usr'];
//        $this->time = $_POST['time'];
//        $this->card_create = $_POST['card_create'];
//        $this->card_exp = $_POST['card_exp'];
        $this->db->update('card_id_system', $this, array('id' => $id));
    }

    function delete_entry($id) {

        $this->db->delete('card_id_system', array('id' => $id));
    }

    function get_entry_where() {
        $this->db->select('card_id_system.id AS id_card,title,firstname,lastname,identification,positype,block,workline,posi');
        $this->db->from('card_id_system');


        $query = $this->db->get();
        return $query->result();
    }

    public function record_data() {
        $this->db->select('card_id_system.id AS id_card,title,firstname,lastname,identification,positype,block,workline,posi');
        $this->db->from('card_id_system');


        $query = $this->db->get();

        return $query->num_rows();
    }

    public function fetch_cardid($limit, $start, $title, $iden, $firstname, $lastname) {
        $this->db->limit($limit, $start);

        $this->db->select('card_id_system.id AS id_card,title,firstname,lastname,identification,positype,block,workline,posi');
        $this->db->from('card_id_system');
        
        if ($title)
            $this->db->where('card_id_system.title LIKE', '%'.$title.'%');
        if ($iden)
            $this->db->where('card_id_system.identification LIKE', '%'.$iden.'%');
        if ($firstname)
            $this->db->where('card_id_system.firstname LIKE', '%'.$firstname.'%');
        if ($lastname)
            $this->db->where('card_id_system.lastname LIKE', '%'.$lastname.'%');



        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

}
