<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of upload
 *
 * @author Gooberm
 */
class Upload extends CI_Controller {

    //put your code here

    function index() {
        if (isset($_POST['submit'])) {

            $config['upload_path'] = './asset/uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $this->load->library('upload', $config);


            if (!$this->upload->do_upload()) {
                echo $this->upload->display_errors();
            } else {

                $data = array('upload_data' => $this->upload->data());
            }
        }



        $this->load->view('upload', $data);
    }

    function upload_same($id) {

        $data = array('id' => $id);

        if (isset($_POST['submit'])) {
            
            //import helper
            $this->load->helper('file');
            $this->load->helper('directory');

            
            //setting config for helper class
            $config['upload_path'] = './asset/uploads/' . $id . '/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $id;
            $this->load->library('upload', $config);
            
            //check file dir exits
            if (!is_dir($config['upload_path'])) {
                //creat file dir
                mkdir($config['upload_path'], 0777, TRUE);
            }

            //check file upload
            if (read_file($config['upload_path'] . $id . '.jpg') || read_file($config['upload_path'] . $id . '.jpeg')
                    || read_file($config['upload_path'] . $id . '.JPG') || read_file($config['upload_path'] . $id . '.JPEG')
                    || read_file($config['upload_path']. $id . '.gif') || read_file($config['upload_path']. $id . '.GIF')
                    || read_file($config['upload_path'] . $id . '.png') || read_file($config['upload_path'] . $id . '.PNG')) {
                //read all file in directory
                $map = directory_map($config['upload_path'], FALSE, TRUE);
                //explode file for select file type name
                $file = explode('.', $map[0]);
                //rename file as [old_file]_index[.filename]
                rename($config['upload_path'].$map[0]
                        , $config['upload_path'].$id.'_'.sizeof($map).'.'.$file[sizeof($file)-1]);
                
            }

            if (!$this->upload->do_upload()) {
                echo $this->upload->display_errors();
            } else {

                $data['upload_data']= $this->upload->data();
            }
        }



        $this->load->view('upload_same', $data);
    }

}

?>
