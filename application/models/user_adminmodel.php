<?php

class user_adminmodel extends CI_Model {

    var $id = "";
    var $username = "";
    var $password = "";
    var $type = "";

    function get_entry() {
        $result = $this->db->get('user_admin', 10);
        return $result->result();
    }

    function insert_entry() {
        $this->username = $_POST['username'];
        $this->password = $_POST['password'];
        $this->type = $_POST['type'];
        $this->db->insert('user_admin', $this);
    }

    function update_entry($id) {
        $this->username = $_POST['username'];
        $this->password = $_POST['password'];
        $this->type = $_POST['type'];
        $this->db->update('user_admin', $this, array('id' => $id));
    }

    function login() {

        if ($_POST['username'] != NULL && $_POST['password'] != NULL) {
            $query = $this->db->get_where('user_admin', array('username' => $_POST['username'],
                'password' => $_POST['password']));

            if (($query->num_rows()) > 0) {
                // print_r($query->num_rows());
                //  print_r($query->result());
                return $query->result();
            } else {
                redirect("admin/index/error", "refresh");
            }
        } else
            redirect("admin/index/error", "refresh");
    }

    function get_one_entry($id) {
        $this->id = $id;

        $where = array('id' => $this->id);
        $query = $this->db->get_where('user_admin', $where);

        return $query->result();
    }

}
