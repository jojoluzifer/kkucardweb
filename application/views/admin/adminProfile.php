

<div class="user-content">
    <div class="row-fluid">

        <div class="span3">
            <div class="side-profile">

                <div class="user-img">
                    <?php
                    $image_properties = array(
                        'src' => 'asset/img/Administrator.png',
                        'class' => 'img-polaroid',
                    );

                    echo img($image_properties);
                    ?>
                </div>
                <div class="user-profile">
                    <div class="row-fluid">
                        <div class="span3"><p class="user-profile-text">user :</p></div>
                        <div class="span9"><p class="user-profile-text2"><?php echo $user_admin[0]->username; ?></p></div>
                        <div align="center">
                            <input type="button" class="btn" value="ออกจากระบบ" onclick="window.location.href='<?= base_url() ?>admin/logout'"/>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <div class="span9">
            <div class="user-tool">
                <div  class="tab-link">
                    <ui >
                        <li class="active" ><a href="<?php echo base_url() ?>admin/adminprofile">รายงานการเข้าใช้ของบุคลาการ</a></li>
                        <li ><a href="<?php echo base_url() ?>admin/admin_show_cardid">รายงานการแก้ไขข้อมูลของบุคลาการ</a></li>
                        <li ><a href="<?php echo base_url() ?>admin/admin_show_bug">รายงานการแจ้งข้อผิดพลาด</a></li>

                    </ui>
                </div>
                <div class="" id="home">
                    <p>&nbsp;</p>
                    <form  method="post" action="<?php echo base_url(''); ?>admin/adminProfile" >
                        <input type="text" class="input-medium search-query" style="width: 10%" placeholder="คำนำหน้าชื่อ" name="title">
                        <input type="text" class="input-medium search-query" style="width: 10%" placeholder="ชื่อ" name="firstname">
                        <input type="text" class="input-medium search-query" style="width: 10%" placeholder="นามสกุล" name="lastname">
                        <input type="text" class="input-medium search-query" style="width: 13%"  placeholder="เลขบัตรประชาชน" name="identification">
                        <input type="text" class="input-medium search-query" style="width: 13%"  placeholder="วันที่ (ปี-เดือน-วัน)" name="time">

                        <button type="submit"  class="btn">Search</button>

                    </form>

                    <form id="tab" class="user-layout">
                        <table class="table table-bordered" >
                            <tr >
                                <td><i class="icon-print"></i>
                                    <?php echo anchor('admin/showlog', ' พิมพ์รายงานการเข้าใช้ของบุคลาการ') ?>
                                </td>
                            </tr>
                        </table>
                        <table class="table table-bordered">
                            <tr>

                                <th>ชื่อ-สกุล</th>
                                <th>เลขบัตรประชาชน </th>
                                <th>การแก้ไขข้อมูลล่าสุด</th>
                                <th>ปรับแต่ง</th>

                            </tr>
                            <?php
                            if ($results) {
                                foreach ($results as $data) {
                                    ?>
                                    <tr>
                                        <td><?php echo $data->title ?>&nbsp;<?php echo $data->firstname ?>&nbsp;<?php echo $data->lastname ?></td>
                                        <td><?php echo $data->identification ?></td>
                                        <td><?php echo $data->date_time ?></td>
                                        <td><a href="<?php echo base_url(); ?>admin/show_logfile/<?php echo $data->log_card_id ?>">แสดงข้อมูล</a></td>

                                    </tr>


                                    <?php
                                }
                            }
                            ?>

                        </table> 
                        <p style="margin-bottom: 15px;"><ul><?php echo $links; ?></ul></p>
                        <i class="icon-print"></i>
                        <?php echo anchor('admin/showlog', ' พิมพ์รายงานการเข้าใช้ของบุคลาการ') ?>
                    </form>
                </div>


            </div>
        </div>


    </div>
</div>