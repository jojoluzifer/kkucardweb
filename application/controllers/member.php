<?php

class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($error = false) {

        $id = $this->session->userdata('id');
        $data['nav_title'] = 'KKU CARD';

        if ($id)
            redirect("member/memberProfile", "refresh");

        $data['page_title'] = '.:: เข้าสู่ระบบ KKU ::.';

        if ($error)
            $data['error'] = "***** ชื่อผู้ใช้งาน หรือ รหัสผ่านของคุณไม่ถูกต้อง *****";
        else
            $data['error'] = "";

        $this->load->view('template/head', $data);
        $this->load->view('template/2_navbar', $data);
        $this->load->view('member/index');
        $this->load->view('template/footer');
    }

    public function memberProfile() {

        $id = $this->session->userdata('id');

        if (!$id)
            redirect("", "refresh");


        $data['page_title'] = '.:: การจัดการข้อมูลของผู้ใช้งาน ::.';
        $data['nav_title'] = 'KKU CARD';
        $data['id'] = $id;


        $this->load->model('card_idmodel', 'model');
        $this->load->model('card_id_systemmodel', 'model2');


        $row = $this->model2->check_entry($id);


        if ($row > 0) {
            $data['user_data'] = $this->model2->get_one_entry($id);
        } else {
            $data['user_data'] = $this->model->get_one_entry($id);
        }

        $this->load->view('template/head', $data);
        $this->load->view('template/2_navbar', $data);
        $this->load->view('member/memberProfile', $data);
    }

    public function login() {
        $data['page_title'] = '.:: การจัดการข้อมูลของผู้ใช้งาน ::.';

        $this->load->model('card_idmodel', 'model');
        $data['card_id'] = $this->model->login();
        
        if ($data['card_id'] != NULL) {
            $this->session->set_userdata('id', $data['card_id'][0]->id);

            $this->load->model('logfilemodel', 'logs');
            $this->logs->insert_entry($data['card_id'][0]->id, 1);
        }
        else { redirect("member/index/error", "refresh"); }
        redirect("member/memberProfile", "refresh");
        /*
          $this->load->view('template/head', $data);
          $this->load->view('member/memberProfile', $data);
         * 
         */
    }

    function logout() {
        $this->session->unset_userdata('id');

        redirect('', 'refresh');
    }

    public function update_data() {

        $data['page_title'] = '.:: แก้ไขข้อมูลผู้ใช้งาน ::.';
        $data['modal_text'] = 'ส่งข้อมูลที่แก้ไขให้ Admin แล้วกรุณารอ Admin ทำการ Update ข้อมูลล่าสุด...';
        $data['id'] = $_POST['id'];

        $this->load->view('template/head', $data);


        $this->load->model('card_id_systemmodel','card_idmodel');
        $row = $this->card_idmodel->check_entry($_POST['id']);

        if ($row > 0) {
            $this->card_idmodel->update_entry($_POST['id']);
        } else {

            $this->card_idmodel->insert_entry();
        }


        $this->load->model('logfilemodel', 'logs');
        $this->logs->insert_entry($_POST['id'], 2);

        $this->load->view('member/memberUpdate', $data);
    }

    public function bug_report() {

        $data['page_title'] = '.:: แจ้งข้อผิดพลาดในการใช้งาน ::.';
        $data['modal_text'] = 'ข้อมูลข้อผิดพลาดได้ถูกส่งแล้ว...';
        $data['id'] = $_POST['id'];
        $this->load->view('template/head', $data);

        $this->load->model('bug_model');

        $this->bug_model->insert_data($_POST['id']);

        $this->load->model('logfilemodel', 'logs');
        $this->logs->insert_entry($_POST['id'], 4);

        $this->load->view('member/memberUpdate', $data);
    }

    function upload_same($id_identification) {

        $data['page_title'] = '.:: อัพโหลดรูปส่วนตัว ::.';
        $data['modal_text'] = 'อัพโหลดรูปส่วนตัว เสร็จสิ้น...';
        $data['id'] = $_POST['id'];
        $this->load->view('template/head', $data);

        $data = array('id_identification' => $id_identification);

        if (isset($_POST['submit'])) {

            //import helper
            $this->load->helper('file');
            $this->load->helper('directory');


            //setting config for helper class
            $config['upload_path'] = './asset/uploads/' . $id_identification . '/';
            $config['allowed_types'] = 'gif|jpg|png';
            //  108px  =  1.5 นิ้ว(inch)    ,   72px = 1 นิ้ว 
            // กำหนด ขนาด image
            $config['max_size'] = '1024';
            $config['max_width'] = '400';
            $config['max_height'] = '600';
            $config['file_name'] = $id_identification;
            $this->load->library('upload', $config);

            //check file dir exits
            if (!is_dir($config['upload_path'])) {
                //creat file dir
                mkdir($config['upload_path'], 0777, TRUE);
            }

            //check file upload
            if (read_file($config['upload_path'] . $id_identification . '.jpg') || read_file($config['upload_path'] . $id_identification . '.jpeg')
                    || read_file($config['upload_path'] . $id_identification . '.JPG') || read_file($config['upload_path'] . $id_identification . '.JPEG')
                    || read_file($config['upload_path'] . $id_identification . '.gif') || read_file($config['upload_path'] . $id_identification . '.GIF')
                    || read_file($config['upload_path'] . $id_identification . '.png') || read_file($config['upload_path'] . $id_identification . '.PNG')) {
                //read all file in directory
                $map = directory_map($config['upload_path'], FALSE, TRUE);
                //explode file for select file type name
                $file = explode('.', $map[0]);
                //rename file as [old_file]_index[.filename]
                rename($config['upload_path'] . $map[0]
                        , $config['upload_path'] . $id_identification . '_' . sizeof($map) . '.' . $file[sizeof($file) - 1]);
            }

            if (!$this->upload->do_upload()) {
                echo $this->upload->display_errors();
            } else {

                $data['upload_data'] = $this->upload->data();
            }
        }

        $this->load->model('logfilemodel', 'logs');
        $this->logs->insert_entry($_POST['id'], 5);
        $this->load->view('member/memberUpload', $data);
    }

}

?>
