<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of card_id_mastermodel
 *
 * @author mege
 */
class card_id_mastermodel extends CI_Model {

    var $id = "";
    var $title = "";
    var $firstname = "";
    var $lastname = "";
    var $sex = "";
    var $birthday = "";
    var $identification = "";
    var $blood = "";
    var $born = "";
    var $nation = "";
    var $religion = "";
    var $statuslist = "";
    var $title_eng = "";
    var $firstname_eng = "";
    var $lastname_eng = "";
    var $putday = "";
    var $positype = "";
    var $block = "";
    var $workline = "";
    var $posi = "";
    var $level = "";
    var $faculty = "";
    var $division = "";
    var $job = "";
    var $unit = "";
    var $homeadd = "";
    var $moo = "";
    var $tok = "";
    var $soi = "";
    var $street = "";
    var $district = "";
    var $amphur = "";
    var $province = "";
    var $telephone = "";
    var $zipcode = "";
    var $email = "";
    var $usr = "";
    var $time = "";


    function get_entry() {
        $result = $this->db->get('card_id_master', 10);
        return $result->result();
    }

    function get_one_entry($id) {


        
        $this->id = $id;


        
        $where = array('id' => $this->id);
        $query = $this->db->get_where('card_id_master', $where);




        //print_r($result->result());

        return $query->result();
    }

    function insert_entry() {
        $this->title = $_POST['title'];
        $this->firstname = $_POST['firstname'];
        $this->lastname = $_POST['lastname'];
        $this->sex = $_POST['sex'];
        $this->birthday = $_POST['birthday'];
        $this->identification = $_POST['identification'];
        $this->blood = $_POST['blood'];
        $this->born = $_POST['born'];
        $this->nation = $_POST['nation'];
        $this->religion = $_POST['religion'];
        $this->statuslist = $_POST['statuslist'];
        $this->title_eng = $_POST['title_eng'];
        $this->firstname_eng = $_POST['firstname_eng'];
        $this->lastname_eng = $_POST['lastname_eng'];
        $this->putday = $_POST['putday'];
        $this->positype = $_POST['positype'];
        $this->block = $_POST['block'];
        $this->workline = $_POST['workline'];
        $this->posi = $_POST['posi'];
        $this->level = $_POST['level'];
        $this->faculty = $_POST['faculty'];
        $this->division = $_POST['division'];
        $this->job = $_POST['job'];
        $this->unit = $_POST['unit'];
        $this->homeadd = $_POST['homeadd'];
        $this->moo = $_POST['moo'];
        $this->tok = $_POST['tok'];
        $this->soi = $_POST['soi'];
        $this->street = $_POST['street'];
        $this->district = $_POST['district'];
        $this->amphur = $_POST['amphur'];
        $this->province = $_POST['province'];
        $this->telephone = $_POST['telephone'];
        $this->zipcode = $_POST['zipcode'];
        $this->email = $_POST['email'];
        $this->usr = $_POST['usr'];
        $this->time = $_POST['time'];
        $this->db->insert('card_id_master', $this);
    }

    function update_entry($id) {
       $title  = '';
        $firstname = '';
        $lastname = '';
        $sex = '';
        $birthday = '';
        $identification = '';
        $blood = '';
        $born = '';
        $nation  = '';
        $religion = '';
        $statuslist = '';
        $title_eng= '';
        $firstname_eng = '';
        $lastname_eng = '';
        $putday = '';
        $positype = '';
        $block = '';
        $workline = '';
        $posi = '';
        $level = '';
        $faculty = '';
        $division = '';
        $unit = '';
        $homeadd = '';
        $moo = '';
        $tok = '';
        $soi = '';
        $job = '';
        $street = '';
        $district = '';
        $amphur = '';
        $province = '';
        $telephone = '';
        $zipcode = '';
        $email = '';
           
    
     
       $_POST['title'] == "-" ?  $title = NULL  : $title = $_POST['title'];
       $_POST['firstname'] == "-" ?  $firstname= NULL  :$firstname =  $_POST['firstname'];
       $_POST['lastname'] == "-" ?  $lastname = NULL  : $lastname = $_POST['lastname'];
       $_POST['sex'] == "-" ?  $sex = NULL  : $sex = $_POST['sex'];
       $_POST['birthday'] == "-" ?  $birthday = NULL  :$birthday =  $_POST['birthday'];
       $_POST['identification'] == "-" ?  $identification = NULL  :$identification=  $_POST['identification'];
       $_POST['blood'] == "-" ?  $blood = NULL  : $blood = $_POST['blood'];
       $_POST['born'] == "-" ?  $born= NULL  : $born = $_POST['born'];
       $_POST['nation'] == "-" ?  $nation= NULL  : $nation = $_POST['nation'];
       $_POST['religion'] == "-" ?  $religion = NULL  : $religion = $_POST['religion'];
       $_POST['statuslist'] == "-" ?  $statuslist = NULL  : $statuslist= $_POST['statuslist'];
       $_POST['title_eng'] == "-" ?  $title_eng = NULL  : $title_eng = $_POST['title_eng'];
       $_POST['firstname_eng'] == "-" ?  $firstname_eng = NULL  : $firstname_eng = $_POST['firstname_eng'];
       $_POST['lastname_eng'] == "-" ?  $lastname_eng = NULL  : $lastname_eng = $_POST['lastname_eng'];
       $_POST['putday'] == "-" ?  $putday = NULL  : $putday= $_POST['putday'];
       $_POST['positype'] == "-" ?  $positype= NULL  : $positype = $_POST['positype'];
       $_POST['block'] == "-" ?  $block = NULL  : $block = $_POST['block'];
       $_POST['workline'] == "-" ?  $workline = NULL  : $workline = $_POST['workline'];
       $_POST['posi'] == "-" ?  $posi = NULL  : $posi = $_POST['posi'];
       $_POST['level'] == "-" ?  $level = NULL  : $level = $_POST['level'];
       $_POST['faculty'] == "-" ?  $faculty = NULL  : $faculty = $_POST['faculty'];
       $_POST['division'] == "-" ?  $division = NULL  : $division = $_POST['division'];
       $_POST['job'] == "-" ?  $job = NULL  : $job = $_POST['job'];
       $_POST['unit'] == "-" ?  $unit = NULL  : $unit = $_POST['unit'];
       $_POST['homeadd'] == "-" ?  $homeadd= NULL  : $homeadd = $_POST['homeadd'];
       $_POST['moo'] == "-" ?  $moo = NULL  : $moo= $_POST['moo'];
       $_POST['tok'] == "-" ?  $tok = NULL  : $tok = $_POST['tok'];
       $_POST['soi'] == "-" ?  $soi = NULL  : $soi = $_POST['soi'];
       $_POST['street'] == "-" ?  $street = NULL  : $street = $_POST['street'];
       $_POST['district'] == "-" ?  $district= NULL  : $district = $_POST['district'];
       $_POST['amphur'] == "-" ?  $amphur= NULL  : $amphur = $_POST['amphur'];
       $_POST['province'] == "-" ?  $province = NULL  : $province = $_POST['province'];
       $_POST['telephone'] == "-" ?  $telephone = NULL  : $telephone = $_POST['telephone'];
       $_POST['zipcode'] == "-" ?  $zipcode= NULL  :$zipcode =  $_POST['zipcode'];
       $_POST['email'] == "-" ?  $email = NULL  : $email = $_POST['email'];
        
      

        $this->id = $_POST['id'];
        $this->title = $title;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->sex = $sex ;
        $this->birthday = $birthday;
        $this->identification = $identification ;
        $this->blood = $blood;
        $this->born = $born;
        $this->nation = $nation;
        $this->religion = $religion ;
        $this->statuslist = $statuslist;
        $this->title_eng = $title_eng ;
        $this->firstname_eng = $firstname_eng;
        $this->lastname_eng = $lastname_eng;
        $this->putday = $putday;
        $this->positype = $positype;
        $this->block = $block;
        $this->workline = $workline ;
        $this->posi = $posi ;
        $this->level = $level;
        $this->faculty = $faculty;
        $this->division = $division;
        $this->job = $job;
        $this->unit = $unit;
        $this->homeadd = $homeadd;
        $this->moo = $moo;
        $this->tok = $tok;
        $this->soi = $soi;
        $this->street = $street;
        $this->district = $district;
        $this->amphur = $amphur;
        $this->province = $province;
        $this->telephone = $telephone;
        $this->zipcode = $zipcode;
        $this->email = $email;
//        $this->usr = $_POST['usr'];
//        $this->time = $_POST['time'];
        $this->db->update('card_id_master', $this, array('id' => $id));
    }

    function login() {
        if ($_POST['username'] != NULL && $_POST['password'] != NULL) {

            $rest_day = substr($_POST['password'], 0, 2) + 0;
            $rest_month = substr($_POST['password'], 2, 2) + 0;
            $rest_yaer = substr($_POST['password'], 4, 8) - 543;

            $biruthday = $rest_yaer . "/" . $rest_month . "/" . $rest_day;
            $this->birthday = $biruthday;

            $query = $this->db->get_where('card_id_master', array('identification' => $_POST['username'],
                'birthday' => $this->birthday));


            
            if (sizeof($query) > 0) {
                // print_r($query->num_rows());
                // print_r($query->result());


                return $query->result();
            } else {
                redirect("member/index/error", "refresh");
            }
        } else {
            redirect("member/index/error", "refresh");
        }
    }
    
    public function admin_update($id){
        
        $this->id = $id;
        
        $where = array('id' => $this->id);
        
       $query =  $this->db->get_where('card_id',$where);
       
       
        
       foreach ($query->result() as $row){
         
           

                $this->title = $row->title  ;
        $this->firstname =  $row->firstname;
        $this->lastname = $row->lastname;
        $this->sex =  $row->sex;
        $this->birthday =  $row->birthday;
        $this->identification =  $row->identification;
        $this->blood = $row->blood;
        $this->born =  $row->born;
        $this->nation = $row->nation;
        $this->religion =  $row->religion;
        $this->statuslist =  $row->statuslist;
        $this->title_eng =  $row->title_eng;
        $this->firstname_eng =  $row->firstname_eng;
        $this->lastname_eng = $row->lastname_eng;
        $this->putday =  $row->putday;
        $this->positype =  $row->positype;
        $this->block =  $row->block;
        $this->workline =  $row->workline;
        $this->posi =  $row->posi;
        $this->level = $row->level;
        $this->faculty = $row->faculty;
        $this->division =  $row->division;
        $this->job = $row->job;
        $this->unit =  $row->unit;
        $this->homeadd = $row->homeadd;
        $this->moo = $row->moo;
        $this->tok =  $row->tok;
        $this->soi =  $row->soi;
        $this->street =  $row->street;
        $this->district =  $row->district;
        $this->amphur =  $row->amphur;
        $this->province =  $row->province;
        $this->telephone =  $row->telephone;
        $this->zipcode =  $row->zipcode;
        $this->email = $row->email;
        
       
           
       }
       $this->db->update('card_id_master', $this, array('id' => $id));
        
        
    }

}

?>
