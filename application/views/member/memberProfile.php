<?php ?>



<div class="user-content">
    <div class="row-fluid">

        <div class="span3">
            <div class="side-profile">

                <div class="user-img">
<!--                                <img src="asset/img/honey.jpg" class="img-polaroid">-->
                    <?php
                    $this->load->helper('directory');
                    $identification = $user_data[0]->identification;
                    $map_image = directory_map('./asset/uploads/' . $identification . '/', 1);
                    //          print_r($map_image);
//                                        $image_identification = array(
//                                            'src' => './asset/uploads/' . $identification . '/' . $map_image[0],
//                                            'class' => 'img-polaroid'
//                                            , 'width' => '72px'
//                                            , 'height' => '90px'
//                                        );
                    //        print_r($image_identification);
//                                        $image_default = array(
//                                            'src' => './asset/img/default.png',
//                                            'class' => 'img-polaroid',
//                                            'width' => '100px',
//                                            'height' => '150px'
//                                        );
                    if ($map_image != NULL) {
                        //      echo img($image_identification);
                        echo '<img src="' . str_replace('index.php/', '', site_url("asset/uploads/" . $identification . "/" . $map_image[0])) . '"  style="width:72px; height:90px"/>';
                    } else {
                        //    echo img($image_default);
                        echo '<img src="' . str_replace('index.php/', '', site_url("asset/img/default.png")) . '"  style="width:72px; height:90px"/>';
                    }
                    ?>
                </div>
                <div class="user-profile">
                    <div class="row-fluid">
                        <div class="span6"><p class="user-profile-text">ชื่อ :</p></div>
                        <div class="span6"><p class="user-profile-text2"><?php echo $user_data[0]->firstname ?></p></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6"><p class="user-profile-text">Username :</p></div>
                        <div class="span6"><p class="user-profile-text2" ><?php echo $user_data[0]->identification ?></p></div>
                    </div>

                    <div align="center">
                        <input type="button" class="btn" value="ออกจากระบบ" onclick="window.location.href='<?= base_url() ?>member/logout'"/>
                    </div>

                </div>

            </div>

        </div>
        <div class="span9">
            <div class="user-tool">
                <div class="tabbable"> <!-- Only required for left/right tabs -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="well">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#home" data-toggle="tab">ข้อมูลส่วนตัว</a></li>
                                    <li><a href="#image" data-toggle="tab">อัพโหลดรูป</a></li>
                                    <li><a href="#print" data-toggle="tab">พิมพ์ข้อมูล</a></li>
                                    <li><a href="#bug" data-toggle="tab">แจ้งข้อผิดพลาด</a></li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane active in" id="home">
                                        <form id="tab" method="post" class="user-layout" action="<?php echo base_url(); ?>member/update_data">
                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>คำนำหน้าชื่อ :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <select name="title">
                                                                <?php if ($user_data[0]->title == 'นาย') { ?>
                                                                    <option value="นาย"selected="selected" >นาย</option>
                                                                    <option value="นาง">นาง</option>
                                                                    <option value = "นางสาว">นางสาว</option>
                                                                <?php } elseif ($user_data[0]->title == 'นาง') { ?> 
                                                                    <option value="นาย">นาย</option>
                                                                    <option value="นาง" selected="selected">นาง</option>
                                                                    <option value="นางสาว">นางสาว</option>
                                                                <?php } else { ?>
                                                                    <option value="นาย" >นาย</option>
                                                                    <option value="นาง">นาง</option>
                                                                    <option value = "นางสาว" selected="selected">นางสาว</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >

                                                </div>                    
                                            </div>

                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>ชื่อจริง :</label>
                                                        </div>
                                                        <div class="span9">
                                                            <input name="firstname" type="text" value="<?php echo $user_data[0]->firstname == "" ? "-" : $user_data[0]->firstname; ?>" class="input-xlarge">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>นามสกุล :</label>
                                                        </div>
                                                        <div class="span9">
                                                            <input name="lastname"  type="text" value="<?php echo $user_data[0]->lastname == "" ? "-" : $user_data[0]->lastname; ?>" class="input-xlarge">
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>Title :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <select name="title_eng">
                                                                <?php if ($user_data[0]->title_eng == 'Mr.') { ?>
                                                                    <option value="Mr."selected="selected" >Mr.</option>
                                                                    <option value="Mrs.">Mrs.</option>
                                                                    <option value = "Miss.">Miss.</option>
                                                                <?php } elseif ($user_data[0]->title_eng == 'Mrs.') { ?> 
                                                                    <option value="Mr.">Mr.</option>
                                                                    <option value="Mrs." selected="selected">Mrs.</option>
                                                                    <option value="Miss.">Miss.</option>
                                                                <?php } else { ?>
                                                                    <option value="Mr." >Mr.</option>
                                                                    <option value="Mrs.">Mrs.</option>
                                                                    <option value = "Miss." selected="selected">Miss.</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >

                                                </div>                    
                                            </div>
                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>First Name :</label>
                                                        </div>
                                                        <div class="span9">
                                                            <input name="firstname_eng" type="text" value="<?php echo $user_data[0]->firstname_eng == "" ? "-" : $user_data[0]->firstname_eng; ?>" class="input-xlarge">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>Last Name :</label>
                                                        </div>
                                                        <div class="span9">
                                                            <input name="lastname_eng" type="text" value="<?php echo $user_data[0]->lastname_eng == "" ? "-" : $user_data[0]->lastname_eng; ?>" class="input-xlarge">
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>เพศ :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <select name="sex">
                                                                <?php if ($user_data[0]->sex == 'ชาย') { ?>
                                                                    <option value="ชาย" selected="selected" >ชาย</option>
                                                                    <option value="หญิง">หญิง</option>
                                                                <?php } else { ?>
                                                                    <option value="ชาย" >ชาย</option>
                                                                    <option value="หญิง" selected="selected">หญิง</option>
                                                                <?php } ?>
                                                            </select>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>เลขบัตรประชาชน :</label>
                                                        </div>
                                                        <div class="span6">

                                                            <input type="text" readonly="true"  name="identification"  value="<?php echo $user_data[0]->identification ?>">
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>วันเกิด :</label>
                                                        </div>
                                                        <div class="span5">

                                                            <input type="text" readonly="true"  name="birthday"  value="<?php echo $user_data[0]->birthday ?>">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>หมู่เลือด :</label>
                                                        </div>
                                                        <div class="span2">
<!--                                                            <select name="blood">
                                                                <?php if ($user_data[0]->blood == 'A') { ?>
                                                                    <option value="A"selected="selected" >A</option>
                                                                    <option value="B">B</option>
                                                                    <option value = "AB">AB</option>
                                                                    <option value = "O">O</option>
                                                                <?php } elseif ($user_data[0]->blood == 'B') { ?> 
                                                                    <option value="A" >A</option>
                                                                    <option value="B" selected="selected">B</option>
                                                                    <option value = "AB">AB</option>
                                                                    <option value = "O">O</option>
                                                                <?php } elseif ($user_data[0]->blood == 'AB') { ?> 
                                                                    <option value="A" >A</option>
                                                                    <option value="B">B</option>
                                                                    <option value = "AB" selected="selected">AB</option>
                                                                    <option value = "O">O</option>
                                                                <?php } else { ?>
                                                                    <option value="A" >A</option>
                                                                    <option value="B">B</option>
                                                                    <option value = "AB">AB</option>
                                                                    <option value = "O" selected="selected">O</option>
                                                                <?php } ?>
                                                            </select>-->
                                                              <input name="blood" type="text" value="<?php echo $user_data[0]->blood == "" ? "-" : $user_data[0]->blood; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>สถานที่เกิด :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input name="born" type="text" value="<?php echo $user_data[0]->born == "" ? "-" : $user_data[0]->born; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>ศาสนา :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="religion" type="text" value="<?php echo $user_data[0]->religion == "" ? "-" : $user_data[0]->religion; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>สัญชาติ :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input name="nation" type="text" value="<?php echo $user_data[0]->nation == "" ? "-" : $user_data[0]->nation; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>สถานะปฏิบัติงาน :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="statuslist" type="text" value="<?php echo $user_data[0]->statuslist == "" ? "-" : $user_data[0]->statuslist; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>วันบรรจุ:</label>
                                                        </div>
                                                        <div class="span3">

                                                            <input type="text" readonly="true" name="putday"  value="<?php echo $user_data[0]->putday ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>ประเภทบุคลากร :</label>
                                                        </div>
                                                        <div class="span6">
<!--                                                            <select name="positype" readonly="true">
                                                                <option >ลูกจ้างชั่วคราว</option>
                                                                <option >ลูกจ้างประจำ</option>
                                                                <option >ข้าราชการ</option>
                                                            </select>-->
                                                             <input type="text" readonly="true" name="positype"  value="<?php echo $user_data[0]->positype ?>">
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>สายงาน :</label>
                                                        </div>
                                                        <div class="span6">
<!--                                                            <select name="workline" readonly="true">
                                                                <option >สายสนับสนุน</option>
                                                                <option >สายผู้สอน</option>

                                                            </select>-->
                                                            <input type="text" readonly="true" name="workline"  value="<?php echo $user_data[0]->workline ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>ตำแหน่ง :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="posi" type="text" value="<?php echo $user_data[0]->posi == "" ? "-" : $user_data[0]->posi; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>กลุ่ม :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input readonly="true" name="block" type="text" value="<?php echo $user_data[0]->block == "" ? "-" : $user_data[0]->block; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>ระดับ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="level" type="text" value="<?php echo $user_data[0]->level == "" ? "-" : $user_data[0]->level; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>คณะ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="faculty" type="text" value="<?php echo $user_data[0]->faculty == "" ? "-" : $user_data[0]->faculty; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>ภาค :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="division" type="text" value="<?php echo $user_data[0]->division == "" ? "-" : $user_data[0]->division; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>JOB :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input readonly="true"  name="job" type="text" value="<?php echo $user_data[0]->job == "" ? "-" : $user_data[0]->job; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>UNIT :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="unit" type="text" value="<?php echo $user_data[0]->unit == "" ? "-" : $user_data[0]->unit; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>บ้านเลขที่ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="homeadd" type="text" value="<?php echo $user_data[0]->homeadd == "" ? "-" : $user_data[0]->homeadd; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span2" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>หมู่ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="moo" type="text" value="<?php echo $user_data[0]->moo == "" ? "-" : $user_data[0]->moo; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>      
                                                <div class="span2" >
                                                    <div class="row-fluid">
                                                        <div class="span5">
                                                            <label>ตรอก :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="tok" type="text" value="<?php echo $user_data[0]->tok == "" ? "-" : $user_data[0]->tok; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span2">
                                                            <label>ซอย :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="soi" type="text" value="<?php echo $user_data[0]->soi == "" ? "-" : $user_data[0]->soi; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-fluid" >
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>ถนน :</label>
                                                        </div>
                                                        <div class="span7">
                                                            <input name="street" type="text" value="<?php echo $user_data[0]->street == "" ? "-" : $user_data[0]->street; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>ตำบล :</label>
                                                        </div>
                                                        <div class="span7">
                                                            <input name="district" type="text" value="<?php echo $user_data[0]->district == "" ? "-" : $user_data[0]->district; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>อำเภอ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="amphur" type="text" value="<?php echo $user_data[0]->amphur == "" ? "-" : $user_data[0]->amphur; ?>"  >
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>

                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>จังหวัด :</label>
                                                        </div>
                                                        <div class="span7">
                                                            <input name="province" type="text" value="<?php echo $user_data[0]->province == "" ? "-" : $user_data[0]->province; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4" >
                                                    <div class="row-fluid">
                                                        <div class="span5">
                                                            <label>รหัสไปรษณีย์ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="zipcode" type="text" value="<?php echo $user_data[0]->zipcode == "" ? "-" : $user_data[0]->zipcode; ?>"  >
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>E-Mail :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="email" type="text" value="<?php echo $user_data[0]->email == "" ? "-" : $user_data[0]->email; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>       
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span2">
                                                            <label>เบอร์โทร :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input name="telephone" type="text" value="<?php echo $user_data[0]->telephone == "" ? "-" : $user_data[0]->telephone; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="id"  value="<?php echo $id ?>">
                                            <p>&nbsp;</p>
                                            <p>&nbsp;</p>
                                            <div>
                                                <input type="submit" class="btn btn-info" value="ปรับปรุงข้อมูล">
                                                <input type="reset" class="btn btn-danger" value="ยกเลิกการปรับปรุง"/>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="image" align="center">
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p align="center" ><font color="#FF000D" >เงื่อนการอัพโหลดรูป ขนาดของรูป 1 นิ้ว ไม่เกิน 400x600pixel ขนาดไฟล์ไม่เกิน 500 KB</font></p>
                                        <p><font color="#FF000D" >ประเภทของไฟล์รูป  .gif | .jpg | .png</font></p>
                                        <p>&nbsp;</p>
                                        <?php
                                        $this->load->helper('directory');
                                        $identification = $user_data[0]->identification;
                                        $map_image = directory_map('./asset/uploads/' . $identification . '/', 1);

                                        if ($map_image != NULL) {
                                            //      echo img($image_identification);
                                            echo '<img src="' . str_replace('index.php/', '', site_url("asset/uploads/" . $identification . "/" . $map_image[0])) . '"  style="width:400px; height:600px"/>';
                                        } else {
                                            //    echo img($image_default);
                                            echo '<img src="' . str_replace('index.php/', '', site_url("asset/img/default.png")) . '"  style="width:400px; height:600px"/>';
                                        }
                                        ?>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <?php echo form_open_multipart('member/upload_same/' . $user_data[0]->identification); ?>

                                        <input type="file" name="userfile" size="20" />  <input type="submit" name="submit" value="upload" />
                                        <input type="hidden" name="id"  value="<?php echo $id ?>">
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="print" align="center">
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p align="center">กรุณาตรวจสอบข้อมูลของท่านก่อนทำการพิมพ์แบบยืนยันข้อมูล</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <input type="button" class="bnt btn-info" value="พิมพ์แบบยืนยันข้อมูล" onclick="window.location.href='<?= base_url() ?>report/printOut/<?= $id ?>'"/>
                                    </div>
                                    <div class="tab-pane fade" id="bug">
                                        <form id="tab3" class="form-inline" method="post" action="<?php echo base_url(); ?>member/bug_report">
                                            <div class="user-bug">
                                                <textarea name="bug-report" placeholder="กรอกข้อความของคุณที่นี่..." rows="10" style="width: 100%;"></textarea>
                                                <div class="user-bug-btn">
                                                    <button class="btn btn-info" type="submit">ส่งข้อความ</button>
                                                    <button  type="reset" >เคลียร์ค่า</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="id"  value="<?php echo $id ?>">


                                        </form>
                                    </div>
                                    

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

