<div class="wrapper">


    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="container">

                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <!-- Be sure to leave the brand out there if you want it shown -->
                <a class="brand" href="JavaScript:history.back();">KKU CARD Administor</a>

                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="active">
                            <a href="JavaScript:history.back();">Home </a>


                        </li>

                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="user-content">
        <div class="row-fluid">
            <!--
                        <div class="span3">-->
            <!--                <div class="side-profile">
            
                                <div class="user-img">
                                            <img src="asset/img/honey.jpg" class="img-polaroid">
            <?php
            $image_properties = array(
                'src' => 'asset/img/honey.jpg',
                'class' => 'img-polaroid',
            );

            echo img($image_properties);
            ?>
                                </div>
                                <div class="user-profile">
                                    <div class="row-fluid">
                                        <div class="span3"><p class="user-profile-text">ชื่อ</p></div>
                                        <div class="span9"><p class="user-profile-text2">ฮันนี่</p></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3"><p class="user-profile-text">อายุ</p></div>
                                        <div class="span9"><p class="user-profile-text2">25</p></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span3"><p class="user-profile-text">ที่อยู่</p></div>
                                        <div class="span9"><p class="user-profile-text2">บ้านกุดนางทุย</p></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span4"><p class="user-profile-text">เบอร์โทร</p></div>
                                        <div class="span8"><p class="user-profile-text2">0809999999</p></div>
                                    </div>
                                </div>
            
                            </div>-->

            <!--     </div>-->
            <div class="">
                <div class="user-tool">
                    <div class="tabbable"> <!-- Only required for left/right tabs -->

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">

                                <div class="well">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#home" data-toggle="tab">Confirm ข้อมูลบุคลากร</a></li>
                                        <!--
                                                                                <li><a href="#print" data-toggle="tab">พิมพ์ข้อมูล</a></li>
                                                                                <li><a href="#bug" data-toggle="tab">แจ้งบัค</a></li>-->
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div class="tab-pane active in" id="home">
                                            <form id="tab" method="post" class="user-layout" action="<?php echo base_url(); ?>admin/admin_update_member/<?php echo $user_data[0]->id; ?>"   >
                                                <div class="row-fluid" >
                                                    <div class="span6">
                                                        <div class="row-fluid">
                                                            <div class="span3">
                                                                <label>คำนำหน้าชื่อ :</label>
                                                            </div>
                                                            <div class="span3">
                                                                <!--<label><?php echo $user_data[0]->title; ?></label>-->
                                                                  <input name="title" readonly="true"  type="text" value="<?php echo $user_data[0]->title == "" ? "-" : $user_data[0]->title; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span6" >

                                                    </div>                    
                                                </div>

                                                <div class="row-fluid" style="margin-bottom: 20px;">
                                                    <div class="span6">
                                                        <div class="row-fluid">
                                                            <div class="span3">
                                                                <label>ชื่อจริง :</label>
                                                            </div>
                                                            <div class="span3">
                                                                <!--<label> <?php echo $user_data[0]->firstname == "" ? "-" : $user_data[0]->firstname; ?></label>-->
                                                                <input name="firstname" readonly="true"  type="text" value="<?php echo $user_data[0]->firstname == "" ? "-" : $user_data[0]->firstname; ?>" class="input-xlarge">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span6" >
                                                        <div class="row-fluid">
                                                            <div class="span3">
                                                                <label>นามสกุล :</label>
                                                            </div>
                                                            <div class="span9">
                                                                <!--<label><?php echo $user_data[0]->lastname == "" ? "-" : $user_data[0]->lastname; ?></label>-->                                                          
                                                                <input name="lastname" readonly="true"  type="text" value="<?php echo $user_data[0]->lastname == "" ? "-" : $user_data[0]->lastname; ?>" class="input-xlarge">
                                                            </div>
                                                        </div>
                                                    </div>                    
                                                </div>

                                                <div class="row-fluid" >
                                                    <div class="span6">
                                                        <div class="row-fluid">
                                                            <div class="span3">
                                                                <label>Title :</label>
                                                            </div>
                                                            <div class="span3">
                                                                <input name="title_eng" readonly="true"  type="text" value="<?php echo $user_data[0]->title_eng == "" ? "-" : $user_data[0]->title_eng; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span6" >

                                                    </div>                    
                                                </div>
                                                <div class="row-fluid" style="margin-bottom: 20px;">
                                                    <div class="span6">
                                                        <div class="row-fluid">
                                                            <div class="span3">
                                                                <label>First Name :</label>
                                                            </div>
                                                            <div class="span9">
                                                                <input name="firstname_eng" readonly="true"  type="text" value="<?php echo $user_data[0]->firstname_eng == "" ? "-" : $user_data[0]->firstname_eng; ?>" class="input-xlarge">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span6" >
                                                        <div class="row-fluid">
                                                            <div class="span3">
                                                                <label>Last Name :</label>
                                                            </div>
                                                            <div class="span9">
                                                                <input name="lastname_eng" readonly="true"  type="text" value="<?php echo $user_data[0]->lastname_eng == "" ? "-" : $user_data[0]->lastname_eng; ?>" class="input-xlarge">
                                                            </div>
                                                        </div>
                                                    </div>                    
                                                </div>
                                                <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>เพศ :</label>
                                                        </div>
                                                        <div class="span1">
                                                             <input type="text" readonly="true"  name="sex"  value="<?php echo $user_data[0]->sex ?>">
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>เลขบัตรประชาชน :</label>
                                                        </div>
                                                        <div class="span4">
                                                            <input type="text" readonly="true"  name="identification"  value="<?php echo $user_data[0]->identification ?>">
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                                <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>วันเกิด :</label>
                                                        </div>
                                                        <div class="span3">

                                                            <input type="text" readonly="true"  name="birthday"  value="<?php echo $user_data[0]->birthday ?>">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>หมู่เลือด :</label>
                                                        </div>
                                                        <div class="span1">
                                                               <input type="text" name="blood"  readonly="true"   value="<?php echo $user_data[0]->blood ?>">
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                                <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>สถานที่เกิด :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input name="born" type="text" readonly="true"  value="<?php echo $user_data[0]->born == "" ? "-" : $user_data[0]->born; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>ศาสนา :</label>
                                                        </div>
                                                        <div class="span1">
                                                            <input name="religion" type="text" readonly="true"  value="<?php echo $user_data[0]->religion == "" ? "-" : $user_data[0]->religion; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                               <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>สัญชาติ :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input readonly="true" name="nation" type="text" value="<?php echo $user_data[0]->nation == "" ? "-" : $user_data[0]->nation; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>สถานะปฏิบัติงาน :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="statuslist" type="text" value="<?php echo $user_data[0]->statuslist == "" ? "-" : $user_data[0]->statuslist; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>

                                                <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>วันบรรจุ:</label>
                                                        </div>
                                                        <div class="span3">

                                                            <input type="text" readonly="true" name="putday"  value="<?php echo $user_data[0]->putday ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>ประเภทบุคลากร :</label>
                                                        </div>
                                                        <div class="span6">
                                                               <input type="text" readonly="true" name="putday"  value="<?php echo $user_data[0]->positype ?>">
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                                <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>สายงาน :</label>
                                                        </div>
                                                        <div class="span6">
                                                              <input type="text" readonly="true" name="putday"  value="<?php echo $user_data[0]->workline ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>ตำแหน่งงาน :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="posi" type="text" value="<?php echo $user_data[0]->posi == "" ? "-" : $user_data[0]->posi; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>กลุ่ม :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input readonly="true" name="block" type="text" value="<?php echo $user_data[0]->block == "" ? "-" : $user_data[0]->block; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>ระดับ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="level" type="text" value="<?php echo $user_data[0]->level == "" ? "-" : $user_data[0]->level; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                                <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>คณะ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="faculty" type="text" value="<?php echo $user_data[0]->faculty == "" ? "-" : $user_data[0]->faculty; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>ภาค :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="division" type="text" value="<?php echo $user_data[0]->division == "" ? "-" : $user_data[0]->division; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>
                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>JOB :</label>
                                                        </div>
                                                        <div class="span3">
                                                            <input readonly="true"  name="job" type="text" value="<?php echo $user_data[0]->job == "" ? "-" : $user_data[0]->job; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>UNIT :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="unit" type="text" value="<?php echo $user_data[0]->unit == "" ? "-" : $user_data[0]->unit; ?>" >
                                                        </div>
                                                    </div>
                                                </div>                    
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>บ้านเลขที่ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="homeadd" type="text" value="<?php echo $user_data[0]->homeadd == "" ? "-" : $user_data[0]->homeadd; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span2" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>หมู่ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="moo" type="text" value="<?php echo $user_data[0]->moo == "" ? "-" : $user_data[0]->moo; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>      
                                                <div class="span2" >
                                                    <div class="row-fluid">
                                                        <div class="span5">
                                                            <label>ตรอก :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="tok" type="text" value="<?php echo $user_data[0]->tok == "" ? "-" : $user_data[0]->tok; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span2">
                                                            <label>ซอย :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="soi" type="text" value="<?php echo $user_data[0]->soi == "" ? "-" : $user_data[0]->soi; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-fluid" >
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>ถนน :</label>
                                                        </div>
                                                        <div class="span7">
                                                            <input readonly="true" name="street" type="text" value="<?php echo $user_data[0]->street == "" ? "-" : $user_data[0]->street; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span2">
                                                            <label>ตำบล :</label>
                                                        </div>
                                                        <div class="span7">
                                                            <input readonly="true" name="district" type="text" value="<?php echo $user_data[0]->district == "" ? "-" : $user_data[0]->district; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4" >
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>อำเภอ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="amphur" type="text" value="<?php echo $user_data[0]->amphur == "" ? "-" : $user_data[0]->amphur; ?>"  >
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>

                                            <div class="row-fluid" style="margin-bottom: 20px;">
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>จังหวัด :</label>
                                                        </div>
                                                        <div class="span7">
                                                            <input readonly="true" name="province" type="text" value="<?php echo $user_data[0]->province == "" ? "-" : $user_data[0]->province; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span4" >
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label>รหัสไปรษณีย์ :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="zipcode" type="text" value="<?php echo $user_data[0]->zipcode == "" ? "-" : $user_data[0]->zipcode; ?>"  >
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>

                                            <div class="row-fluid" >
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span3">
                                                            <label>E-Mail :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="email" type="text" value="<?php echo $user_data[0]->email == "" ? "-" : $user_data[0]->email; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>       
                                                <div class="span6">
                                                    <div class="row-fluid">
                                                        <div class="span2">
                                                            <label>เบอร์โทร :</label>
                                                        </div>
                                                        <div class="span6">
                                                            <input readonly="true" name="telephone" type="text" value="<?php echo $user_data[0]->telephone == "" ? "-" : $user_data[0]->telephone; ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                                <input type="text" style="display: none;" name="id"  value="<?php echo $user_data[0]->id; ?>">





                                                <div>
                                                    <input type="submit" class="btn btn-info" value="Confirm">
                                                </div>
                                            </form>
                                        </div>
                                        <!--                                        <div class="tab-pane fade" id="print">
                                                                                    <form id="tab2">
                                        
                                                                                    </form>
                                                                                </div>-->
                                        <!--                                        <div class="tab-pane fade" id="bug">
                                                                                    <form id="tab3" class="form-inline">
                                                                                        <div class="user-bug">
                                                                                            <textarea placeholder="กรอกข้อความของคุณที่นี่..." rows="10" style="width: 100%;"></textarea>
                                                                                            <div class="user-bug-btn">
                                                                                                <button class="btn btn-info" type="submit">ส่งข้อความ</button>
                                                                                                <button class="btn" type="submit">เคลียร์ค่า</button>
                                                                                            </div>
                                                                                        </div>
                                        
                                        
                                                                                    </form>
                                                                                </div>-->

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>





    </div>