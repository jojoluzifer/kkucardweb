<?php

class Bug_model extends CI_Model {

    var $bug_id = "";
    var $id = "";
    var $text = "";
    var $time = "";

    function __construct() {
        parent::__construct();
    }

    public function insert_data($id) {

        $this->id = $id;
        $where = array('id' => $this->id);

        $this->id = $id;
        $this->text = $_POST['bug-report'];
        $this->time = date("Y-m-d H:i:s");

        $this->db->insert('bug', $this, $where);
    }

    function get_entry_where() {
        $this->db->select('bug.id AS id_bug,text,bug.time as date_time,title,firstname,lastname,identification,positype,block,workline,posi');
        $this->db->from('bug');
        $this->db->join('card_id', 'bug.id = card_id.id');
        $this->db->group_by('bug.id');
        $this->db->order_by('bug.time', 'desc');


        $query = $this->db->get();
        return $query->result();
    }

    function get_one_entry_where($id) {
        $this->db->select('bug.id AS id_bug,text,bug.time as date_time,title,firstname,lastname,identification,positype,block,workline,posi');
        $this->db->from('bug');
        $this->db->join('card_id', 'bug.id = card_id.id');
        $this->db->where('card_id.id', $id);
        $this->db->order_by('bug.time', 'desc');

        $result = $this->db->get();
        return $result->result();
    }

    public function record_data() {
        $this->db->select('bug.id AS id_bug,text,bug.time as date_time,title,firstname,lastname,identification,positype,block,workline,posi');
        $this->db->from('bug');
        $this->db->join('card_id', 'bug.id = card_id.id');
        $this->db->group_by('bug.id');
        $this->db->order_by('bug.time', 'desc');

        $query = $this->db->get();

        return $query->num_rows();
    }

    public function fetch_bug($limit, $start, $title, $iden, $firstname, $lastname, $time) {
        $this->db->limit($limit, $start);


        $this->db->select('bug.id AS id_bug,text,bug.time as date_time,title,firstname,lastname,identification,positype,block,workline,posi');
        $this->db->from('bug');
        $this->db->join('card_id', 'bug.id = card_id.id');
        $this->db->group_by('bug.id');
        $this->db->order_by('bug.time', 'desc');

        if ($title)
            $this->db->where('card_id.title LIKE', '%'.$title.'%');
        if ($iden)
            $this->db->where('card_id.identification LIKE', '%'.$iden.'%');
        if ($firstname)
            $this->db->where('card_id.firstname LIKE ', '%'.$firstname.'%');
        if ($lastname)
            $this->db->where('card_id.lastname LIKE', '%'.$lastname.'%');
        if ($time)
            $this->db->where('bug.time LIKE', '%'.$time.'%');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

}

?>
