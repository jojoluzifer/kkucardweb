<?php

class Blog extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->load->model('blog_model', '', true);

        $data['query'] = $this->blog_model->get_last_ten_entries();

        $this->load->view('blogview', $data);
    }

    public function test() {
        $this->load->model('card_id_mastermodel', 'model');
        $data['card_id'] = $this->model->get_entry();

        $this->load->view('template/head', $data);
        $this->parser->parse('member/test',$data);
    }

}

sdad
?>
