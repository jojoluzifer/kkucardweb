
<div class="user-content">
    <div class="row-fluid">

        <div class="span3">
            <div class="side-profile">

                <div class="user-img">
                    <?php
                    $image_properties = array(
                        'src' => 'asset/img/Administrator.png',
                        'class' => 'img-polaroid',
                    );

                    echo img($image_properties);
                    ?>
                </div>
                <div class="user-profile">
                    <div class="row-fluid">
                        <div class="span3"><p class="user-profile-text">user :</p></div>
                        <div class="span9"><p class="user-profile-text2"><?php echo $user_admin[0]->username; ?></p></div>
                        <div align="center">
                            <input type="button" class="btn" value="ออกจากระบบ" onclick="window.location.href='<?= base_url() ?>admin/logout'"/>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <div class="span9">
            <div class="user-tool">
                <div  class="tab-link">
                    <ui >
                        <li  ><a href="<?php echo base_url() ?>admin/adminprofile">รายงานการเข้าใช้ของบุคลาการ</a></li>
                        <li ><a href="<?php echo base_url() ?>admin/admin_show_cardid">รายงานการแก้ไขข้อมูลของบุคลาการ</a></li>
                        <li class="active"><a href="<?php echo base_url() ?>admin/admin_show_bug">รายงานการแจ้งข้อผิดพลาด</a></li>

                    </ui>
                </div>
                <div class="" id="bug">
                    <p>&nbsp;</p>
                    <form  method="post" action="<?php echo base_url(''); ?>admin/admin_show_bug" >
                        <input type="text" class="input-medium search-query" style="width: 10%" placeholder="คำนำหน้าชื่อ" name="title">
                        <input type="text" class="input-medium search-query" style="width: 10%" placeholder="ชื่อ" name="firstname">
                        <input type="text" class="input-medium search-query" style="width: 10%" placeholder="นามสกุล" name="lastname">
                        <input type="text" class="input-medium search-query" style="width: 13%"  placeholder="เลขบัตรประชาชน" name="identification">
                        <input type="text" class="input-medium search-query" style="width: 13%"  placeholder="วันที่ (ปี-เดือน-วัน)" name="time">

                        <button type="submit"  class="btn">Search</button>

                    </form>

                    <form id="tab3" class="user-layout">
                        <table class="table table-bordered">

                            <tr>
                                <th>ชื่อ-สกุล</th>
                                <th>เลขบัตรประชาชน </th>
                                <th>วันเวลาที่แจ้งข้อผิดพลาด </th>
<!--                                                        <th>ข้อความ</th>-->
                                <th>ปรับแต่ง</th>
                            </tr>



                            <?php
#Function Cut Word 

                            function cutStr($str, $maxChars = '', $holder = '') {

                                if (strlen($str) > $maxChars) {
                                    $str = iconv_substr($str, 0, $maxChars) . $holder;
                                }
                                return $str;
                            }
                            ?>

                            <?php
                            if ($results_b) {
                                foreach ($results_b as $data) {
                                    ?>
                                    <tr>
                                        <td><?php echo $data->title ?>&nbsp;<?php echo $data->firstname ?>&nbsp;<?php echo $data->lastname ?></td>
                                        <td><?php echo $data->identification ?></td>
                                        <td><?php echo $data->date_time ?></td>
        <!--                                                        <td>
                                        <?php
                                        $detail = "'.{text}.'";
                                        echo cutStr($detail, '5', '...');
                                        ?>
                                        </td>-->
                                        <td><a href="<?php echo base_url() ?>admin/adminShowbug/<?php echo $data->id_bug ?>">แสดงข้อมูล</a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>


                        </table> 
                        <p style="margin-bottom: 15px;"><?php echo $links_b; ?></p>
                    </form>
                </div>


            </div>
        </div>


    </div>
</div>