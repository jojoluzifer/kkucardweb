<table class="table table-bordered">
    <tr>
        <th>ชื่อ-สกุล</th>
        <th>เลขบัตรประชาชน </th>
        <th>ตำแหน่งงาน</th>
        <th>วันเวลาที่แจ้งข้อผิดพลาด </th>
        <th>ข้อความ</th>
    </tr>
    {bug}
    <tr>
        <td>{title}&nbsp;{firstname}&nbsp;{lastname}</td>
        <td>{identification}</td>
        <td>{posi}</td>
        <td>{date_time}</td>
        <td>{text}</td>
    </tr>
{/bug}
</table>