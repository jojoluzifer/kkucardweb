<?php

class logfilemodel extends CI_Model {

    var $logfile_id = "";
    var $id = "";
    var $date_time = "";
    var $log_type = "";

    function get_entry() {
        $result = $this->db->get('logfile', 10);
        return $result->result();
    }

    function insert_entry($id, $log_type) {


        $this->logfile_id = NULL;
        $this->id = $id;
        $this->date_time = date('Y-m-d H:i:s');
        $this->log_type = $log_type;
        $this->db->insert('logfile', $this);
    }

    function update_entry($id) {
        $this->logfile_id = $_POST['logfile_id'];
        $this->date_time = $_POST['date_time'];
        $this->log_type = $_POST['log_type'];
        $this->edit_id = $_POST['edit_id'];
        $this->db->update('logfile', $this, array('id' => $id));
    }

    function get_entry_where() {
        $this->db->select('logfile.id AS log_card_id,title,firstname,lastname,identification,date_time,positype,block,workline,posi');
        $this->db->from('logfile');
        $this->db->join('card_id', 'logfile.id = card_id.id');
        $this->db->group_by('logfile.id');
        $this->db->order_by('logfile.date_time', 'desc');

        $query = $this->db->get();
        return $query->result();
    }

    function get_one_entry_where($id) {
//        echo $this->id = $id;
//
//        $where = array('id' => $this->id);

        $this->db->select('*');
        $this->db->from('logfile');
        $this->db->join('card_id', 'logfile.id = card_id.id');
        $this->db->join('log_type', 'logfile.log_type=log_type.log_type_id');
        $this->db->where('card_id.id', $id);
        $this->db->order_by('logfile.date_time', 'desc');

        $result = $this->db->get();
        return $result->result();
    }

    public function record_data() {
        $this->db->select('logfile.id AS log_card_id,title,firstname,lastname,identification,date_time,positype,block,workline,posi');
        $this->db->from('logfile');
        $this->db->join('card_id', 'logfile.id = card_id.id');
        $this->db->group_by('logfile.id');
        $this->db->order_by('logfile.date_time', 'desc');

        $query = $this->db->get();

        return $query->num_rows();
    }

    public function fetch_logfile($limit, $start, $title, $iden, $firstname, $lastname, $time) {
        $this->db->limit($limit, $start);

        $this->db->select('logfile.id AS log_card_id,title,firstname,lastname,identification,date_time,positype,block,workline,posi');
        $this->db->from('logfile');
        $this->db->join('card_id', 'logfile.id = card_id.id');
        $this->db->group_by('logfile.id');
        $this->db->order_by('logfile.date_time', 'desc');
        
        if ($title)
            $this->db->where('card_id.title LIKE', '%'. $title.'%');
        if ($iden)
            $this->db->where('card_id.identification LIKE','%'. $iden.'%' );
        if ($firstname)
            $this->db->where('card_id.firstname LIKE', '%'. $firstname.'%');
        if ($lastname)
            $this->db->where('card_id.lastname LIKE', '%'. $lastname.'%');
        if ($time)
            $this->db->where('logfile.date_time LIKE', '%'. $time . '%');

        $query = $this->db->get();
    
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

}
